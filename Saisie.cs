﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ChercherLaValeur;
namespace Verif
{
    [TestClass]
    public class Saisie
    {
        [TestMethod]
        public void TestSaisie()
        {
            Assert.AreEqual(0, Program.VerifSaisie("1,2,3,4,5,6,7,8,9,10"));
            Assert.AreEqual(-1, Program.VerifSaisie("-1,-2,-3,-4,-5,-6,-7,-8,-9,-10"));
        }
    }
}

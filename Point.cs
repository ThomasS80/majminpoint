﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fin;
namespace MajMin
{
    [TestClass]
    public class Point
    {
        [TestMethod]
        public void MajOuMin()
        {
            Assert.AreEqual(true, Program.MajusculeMin("Bonjour a tous"));
            Assert.AreEqual(false, Program.MajusculeMin("bonjour a tous"));
        }
        [TestMethod]
        public void TestPoint()
        {
            Assert.AreEqual(true, Program.MajusculeMin("Bonjour a tous."));
            Assert.AreEqual(false, Program.MajusculeMin("bonjour a tous,"));
        }
    }
}

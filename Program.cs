﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fin
{
    public class Program
    {
        public static bool MajusculeMin(string phrase)
        {
            char Deb_Maj = 'A';
            char Deb_Min = 'a';
            if (phrase[0] >= Deb_Maj && phrase[0] <= Deb_Min)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool PointTest(string phrase)
        {
            int length = phrase.Length - 1;
            if (phrase[length] == '.')
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        static void Main(string[] args)
        {

            string phrase;

            Console.WriteLine("Saisissez votre phrase : ");
            phrase = string.Format(Console.ReadLine());

            Console.WriteLine("La phrase que vous avez saisi était : {0}", phrase);
            bool resultat = Program.MajusculeMin(phrase);
            if (resultat == true)
            {
                Console.WriteLine("La phrase commence par une Majuscule");
            }
            else
            {
                Console.WriteLine("La phrase commence par une Minuscule");
            }
            bool resultat_point = Program.PointTest(phrase);
            if (resultat == true)
            {
                Console.WriteLine("La phrase se finit par un point");
            }
            else
            {
                Console.WriteLine("La phrase ne se finit pas par un point");
            }
            Console.ReadKey();
        }
    }
};
